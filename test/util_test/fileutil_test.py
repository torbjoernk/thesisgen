# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import unittest
import os

from test.helper import TestDir

from thesisgen.util.fileutil import comp_checksum


class CheckSumTest(unittest.TestCase):
    def setUp(self):
        self.tmp = TestDir()

    def test_existing_file(self):
        content = "Default Content"
        sha256 = "83935af234e26e42a0d050dbd298068b7b5bc403f11a4ba90575dbba93c726d3"
        tmpfile = self.tmp.create_testfile(content=content)
        self.assertTrue(os.path.exists(tmpfile))
        self.assertEqual(comp_checksum(tmpfile), sha256)

    def test_non_existing_file(self):
        tmpfile = "not_a_file"
        self.assertFalse(os.path.exists(tmpfile))
        with self.assertRaises(RuntimeError):
            comp_checksum(tmpfile)
