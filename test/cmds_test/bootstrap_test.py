# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import os
import unittest

from test.helper import TestDir
from thesisgen.cmds.bootstrap import BootstrapCmd


class BootstrapOptionsTest(unittest.TestCase):
    def setUp(self):
        self.project_dir = os.path.abspath(os.path.join(os.path.realpath(__file__), '..', '..', '..'))
        self.tmpdir = TestDir()

    def test_bootstrap_cmd(self):
        with self.assertLogs(logger='thesisgen') as cm:
            BootstrapCmd(['bootstrap']).run()
        self.assertNotRegex('\n'.join(cm.output), r"\[E\]")
        self.assertRegex('\n'.join(cm.output), "Bootstrapping a Thesis")
        self.assertRegex('\n'.join(cm.output), "Bootstrapping into: %s" % os.getcwd())

    def test_takes_dest_path(self):
        with self.assertLogs(logger='thesisgen') as cm:
            BootstrapCmd(['bootstrap', "--dest=%s" % self.tmpdir.tmpdir]).run()
        self.assertNotRegex('\n'.join(cm.output), r"\[E\]")
        self.assertRegex('\n'.join(cm.output), "Bootstrapping a Thesis")
        self.assertRegex('\n'.join(cm.output), "Bootstrapping into: %s" % self.tmpdir.tmpdir)
