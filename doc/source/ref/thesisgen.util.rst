thesisgen.util
==============

.. automodule:: thesisgen.util

.. autosummary::
    :toctree: .

    thesisgen.util.fileutil
    thesisgen.util.log
