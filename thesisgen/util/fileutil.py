# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import hashlib
import logging
import os

_log = logging.getLogger(__name__)


def comp_checksum(filename):
    if os.path.exists(filename):
        _log.debug("computing SHA256 checksum of '%s'" % filename)
        with open(filename, mode='rb') as fh:
            return hashlib.sha256(fh.read()).hexdigest()
    else:
        raise RuntimeError("File '%s' not found." % filename)
