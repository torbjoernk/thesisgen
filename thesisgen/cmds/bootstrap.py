# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import docopt
import logging
import os

from thesisgen.cmds.command import Command

_log = logging.getLogger(__name__)


class BootstrapCmd(Command):
    """
    Usage: thesisgen bootstrap [--dest=PATH]

    Options:
        --dest=PATH  base path for bootstrapped thesis
    """
    def __init__(self, argv):
        self._dest_dir = None

        super(BootstrapCmd, self).__init__(argv)

    def run(self):
        super(BootstrapCmd, self).run()
        _log.info("Bootstrapping a Thesis ...")
        self._set_dest_path()

    @property
    def dest_dir(self):
        return self._dest_dir

    def _parse_docopt(self, argv):
        super(BootstrapCmd, self)._parse_docopt(argv)
        self._args = docopt.docopt(BootstrapCmd.__doc__, argv=argv)
        _log.debug("bootstrap args: %s" % dict(self._args))

    def _set_dest_path(self):
        if self._args.get('--dest', None) and isinstance(self._args['--dest'], str):
            try:
                if os.path.exists(os.path.abspath(self._args['--dest'])):
                    self._dest_dir = self._args['--dest']
                else:
                    _log.error("Desired directory not readable: %s" % self._args['--dest'])
            except OSError as err:
                _log.error("Desired directory not accessible: %s" % self._args['--dest'])
                _log.critical(err)
        else:
            self._dest_dir = os.getcwd()
        _log.info("Bootstrapping into: %s" % self.dest_dir)
