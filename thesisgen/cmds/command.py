# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""


class Command(object):
    def __init__(self, argv):
        self._args = {}
        self._parse_docopt(argv)

    def run(self):
        pass

    def _parse_docopt(self, argv):
        pass
